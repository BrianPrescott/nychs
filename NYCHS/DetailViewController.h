//
//  DetailViewController.h
//  NYCHS
//
//  Created by Brian Prescott on 6/18/19.
//  Copyright © 2019 Brian Prescott. All rights reserved.
//

#import <UIKit/UIKit.h>

@class School, Scores;

@interface DetailViewController : UIViewController

@property (strong, nonatomic) School *detailSchool;
@property (strong, nonatomic) Scores *detailScores;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

@end

