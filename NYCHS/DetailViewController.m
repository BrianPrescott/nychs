//
//  DetailViewController.m
//  NYCHS
//
//  Created by Brian Prescott on 6/18/19.
//  Copyright © 2019 Brian Prescott. All rights reserved.
//

#import "DetailViewController.h"

#import "School.h"
#import "Scores.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

- (void)configureView
{
    if (self.detailSchool) {
        if (self.detailScores)
        {
            self.detailDescriptionLabel.text = [NSString stringWithFormat:@"%@\n\n%@\n\nSAT Math: %@\n\nSAT Reading: %@\n\nSAT Writing: %@", _detailSchool.school_name, _detailSchool.city, _detailScores.sat_math_avg_score, _detailScores.sat_critical_reading_avg_score, _detailScores.sat_writing_avg_score];
        }
        else
        {
            self.detailDescriptionLabel.text = [NSString stringWithFormat:@"%@\n\n%@\n\nNo SAT scores found", _detailSchool.school_name, _detailSchool.city];
        }
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self configureView];
    
    self.detailDescriptionLabel.textColor = [UIColor blueColor];
}

- (void)setDetailSchool:(School *)newDetailSchool
{
    if (_detailSchool != newDetailSchool) {
        _detailSchool = newDetailSchool;
        [self configureView];
    }
}

- (void)setDetailScores:(Scores *)newDetailScores
{
    if (_detailScores != newDetailScores) {
        _detailScores = newDetailScores;
        [self configureView];
    }
}

@end
