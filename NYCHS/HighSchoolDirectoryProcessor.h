//
//  HighSchoolDirectoryProcessor.h
//  NYCHS
//
//  Created by Brian Prescott on 6/18/19.
//  Copyright © 2019 Brian Prescott. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol HighSchoolDirectoryProcessorDelegate
- (void)highSchoolDirectoryProcessorFinished:(NSArray *_Nonnull)schools;
- (void)highSchoolDirectoryProcessorError:(NSError *_Nonnull)error;
@end

NS_ASSUME_NONNULL_BEGIN

@interface HighSchoolDirectoryProcessor : NSObject

@property (nonatomic, weak) id delegate;

- (void)downloadData;

@end

NS_ASSUME_NONNULL_END
