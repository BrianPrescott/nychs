//
//  HighSchoolDirectoryProcessor.m
//  NYCHS
//
//  Created by Brian Prescott on 6/18/19.
//  Copyright © 2019 Brian Prescott. All rights reserved.
//

#import "HighSchoolDirectoryProcessor.h"

#import "School.h"

@implementation HighSchoolDirectoryProcessor

- (void)downloadData
{
    NSLog(@"Downloading high school data");
    NSURL *url = [NSURL URLWithString:@"https://data.cityofnewyork.us/resource/s3k6-pzi2.json"];
    __weak HighSchoolDirectoryProcessor *weakSelf = self;
    NSURLSessionDataTask *downloadTask = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error)
        {
            if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(highSchoolDirectoryProcessorError:)])
            {
                [weakSelf.delegate highSchoolDirectoryProcessorError:error];
            }
        }
        else
        {
            [self processData:data];
        }
    }];
    
    [downloadTask resume];
}

- (void)processData:(NSData *)data
{
    NSError *e = nil;
    NSArray *arr = [NSJSONSerialization JSONObjectWithData:data options: NSJSONReadingMutableContainers error: &e];
    NSLog(@"Done downloading high school data, record count: %@", @(arr.count));
    NSMutableArray *schools = [NSMutableArray array];
    for (NSDictionary *dict in arr)
    {
        School *school = [[School alloc] init];
        school.dbn = dict[@"dbn"];
        school.school_name = dict[@"school_name"];
        school.city = dict[@"city"];
        [schools addObject:school];
    }

    if (_delegate && [_delegate respondsToSelector:@selector(highSchoolDirectoryProcessorFinished:)])
    {
        [_delegate highSchoolDirectoryProcessorFinished:schools];
    }
}

@end
