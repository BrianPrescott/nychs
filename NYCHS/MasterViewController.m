//
//  MasterViewController.m
//  NYCHS
//
//  Created by Brian Prescott on 6/18/19.
//  Copyright © 2019 Brian Prescott. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"
#import "HighSchoolDirectoryProcessor.h"
#import "School.h"
#import "ScoresProcessor.h"
#import "Scores.h"

@interface MasterViewController ()

@property NSArray *schools;
@property NSArray *scores;
@property HighSchoolDirectoryProcessor *highSchoolDirectoryProcessor;
@property ScoresProcessor *scoresProcessor;
@property UIActivityIndicatorView *activityIndicatorView;

@end

@implementation MasterViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *refreshButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refreshButtonPressed:)];
    self.navigationItem.rightBarButtonItem = refreshButton;
    self.detailViewController = (DetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
    
    _activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    UIBarButtonItem * barButton = [[UIBarButtonItem alloc] initWithCustomView:_activityIndicatorView];
    [self navigationItem].leftBarButtonItem = barButton;
    [_activityIndicatorView startAnimating];
    
    _highSchoolDirectoryProcessor = [[HighSchoolDirectoryProcessor alloc] init];
    _highSchoolDirectoryProcessor.delegate = self;
    
    _scoresProcessor = [[ScoresProcessor alloc] init];
    _scoresProcessor.delegate = self;
    
    [_highSchoolDirectoryProcessor downloadData];
}

- (void)viewWillAppear:(BOOL)animated
{
    self.clearsSelectionOnViewWillAppear = self.splitViewController.isCollapsed;
    [super viewWillAppear:animated];
}

- (void)refreshButtonPressed:(id)sender
{
    if (_activityIndicatorView.isAnimating)
    {
        NSLog(@"Data is already downloading");
        return;
    }
    
    _schools = nil;
    [self.tableView reloadData];
    [_activityIndicatorView startAnimating];
    [_highSchoolDirectoryProcessor downloadData];
}

- (void)displayAlert:(NSString *)title message:(NSString *)message
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - HighSchoolDirectoryProcessorDelegate

- (void)highSchoolDirectoryProcessorFinished:(NSArray *)schools
{
    NSArray *sortedSchools = [schools sortedArrayUsingComparator:^NSComparisonResult(School *s1, School *s2) { return [s1.school_name compare:s2.school_name]; }];
    _schools = sortedSchools;
    [_scoresProcessor downloadData];
}

- (void)highSchoolDirectoryProcessorError:(NSError *)error
{
    _schools = nil;
    __weak MasterViewController *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.activityIndicatorView stopAnimating];
        [weakSelf.tableView reloadData];
        [weakSelf displayAlert:@"Error" message:error.description];
    });
}

#pragma mark - ScoresProcessorDelegate

- (void)scoresProcessorFinished:(NSArray *)scores
{
    _scores = scores;
    __weak MasterViewController *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.activityIndicatorView stopAnimating];
        [weakSelf.tableView reloadData];
    });
}

- (void)scoresProcessorError:(NSError *)error
{
    _schools = nil;
    __weak MasterViewController *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.activityIndicatorView stopAnimating];
        [weakSelf.tableView reloadData];
        [weakSelf displayAlert:@"Error" message:error.description];
    });
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        School *school = _schools[indexPath.row];
        Scores *scores = [Scores findScores:_scores dbn:school.dbn];
        
        DetailViewController *controller = (DetailViewController *)[[segue destinationViewController] topViewController];
        [controller setDetailSchool:school];
        [controller setDetailScores:scores];
        controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        controller.navigationItem.leftItemsSupplementBackButton = YES;
    }
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _schools.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    School *school = _schools[indexPath.row];
    cell.textLabel.text = school.school_name;
    cell.detailTextLabel.text = school.city;
    return cell;
}

@end
