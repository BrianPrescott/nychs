//
//  School.h
//  NYCHS
//
//  Created by Brian Prescott on 6/18/19.
//  Copyright © 2019 Brian Prescott. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface School : NSObject

@property NSString *dbn;
@property NSString *school_name;
@property NSString *city;

+ (School *)findSchool:(NSArray *)schools dbn:(NSString *)dbn;

@end

NS_ASSUME_NONNULL_END
