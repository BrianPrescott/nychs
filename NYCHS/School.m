//
//  School.m
//  NYCHS
//
//  Created by Brian Prescott on 6/18/19.
//  Copyright © 2019 Brian Prescott. All rights reserved.
//

#import "School.h"

@implementation School

+ (School *)findSchool:(NSArray *)schools dbn:(NSString *)dbn
{
    for (School *s in schools)
    {
        if ([s.dbn isEqualToString:dbn])
        {
            return s;
        }
    }
    
    return nil;
}

@end
