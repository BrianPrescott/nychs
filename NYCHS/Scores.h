//
//  Scores.h
//  NYCHS
//
//  Created by Brian Prescott on 6/18/19.
//  Copyright © 2019 Brian Prescott. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Scores : NSObject

@property NSString *dbn;
@property NSString *sat_critical_reading_avg_score;
@property NSString *sat_math_avg_score;
@property NSString *sat_writing_avg_score;

+ (Scores *)findScores:(NSArray *)scores dbn:(NSString *)dbn;

@end

NS_ASSUME_NONNULL_END
