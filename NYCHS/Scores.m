//
//  Scores.m
//  NYCHS
//
//  Created by Brian Prescott on 6/18/19.
//  Copyright © 2019 Brian Prescott. All rights reserved.
//

#import "Scores.h"

@implementation Scores

+ (Scores *)findScores:(NSArray *)scores dbn:(NSString *)dbn
{
    for (Scores *s in scores)
    {
        if ([s.dbn isEqualToString:dbn])
        {
            return s;
        }
    }
    
    return nil;
}

@end
