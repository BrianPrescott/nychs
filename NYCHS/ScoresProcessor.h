//
//  ScoresProcessor.h
//  NYCHS
//
//  Created by Brian Prescott on 6/18/19.
//  Copyright © 2019 Brian Prescott. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ScoresProcessorDelegate
- (void)scoresProcessorFinished:(NSArray *_Nonnull)schools;
- (void)scoresProcessorError:(NSError *_Nonnull)error;
@end

NS_ASSUME_NONNULL_BEGIN

@interface ScoresProcessor : NSObject

@property (nonatomic, weak) id delegate;

- (void)downloadData;

@end

NS_ASSUME_NONNULL_END
