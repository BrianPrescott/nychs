//
//  ScoresProcessor.m
//  NYCHS
//
//  Created by Brian Prescott on 6/18/19.
//  Copyright © 2019 Brian Prescott. All rights reserved.
//

#import "ScoresProcessor.h"

#import "Scores.h"

@implementation ScoresProcessor

- (void)downloadData
{
    NSLog(@"Downloading scores data");
    NSURL *url = [NSURL URLWithString:@"https://data.cityofnewyork.us/resource/f9bf-2cp4.json"];
    __weak ScoresProcessor *weakSelf = self;
    NSURLSessionDataTask *downloadTask = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error)
        {
            if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(scoresProcessorError:)])
            {
                [weakSelf.delegate scoresProcessorError:error];
            }
        }
        else
        {
            [self processData:data];
        }
    }];
    
    [downloadTask resume];
}

- (void)processData:(NSData *)data
{
    NSError *e = nil;
    NSArray *arr = [NSJSONSerialization JSONObjectWithData:data options: NSJSONReadingMutableContainers error: &e];
    NSLog(@"Done downloading scores data, record count: %@", @(arr.count));
    NSMutableArray *scores = [NSMutableArray array];
    for (NSDictionary *dict in arr)
    {
        Scores *score = [[Scores alloc] init];
        score.dbn = dict[@"dbn"];
        score.sat_math_avg_score = dict[@"sat_math_avg_score"];
        score.sat_writing_avg_score = dict[@"sat_writing_avg_score"];
        score.sat_critical_reading_avg_score = dict[@"sat_critical_reading_avg_score"];
        [scores addObject:score];
    }
    
    if (_delegate && [_delegate respondsToSelector:@selector(scoresProcessorFinished:)])
    {
        [_delegate scoresProcessorFinished:scores];
    }
}

@end
