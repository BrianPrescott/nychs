//
//  NYCHSTests.m
//  NYCHSTests
//
//  Created by Brian Prescott on 6/18/19.
//  Copyright © 2019 Brian Prescott. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "HighSchoolDirectoryProcessor.h"
#import "School.h"
#import "ScoresProcessor.h"
#import "Scores.h"

@interface NYCHSTests : XCTestCase
{
    XCTestExpectation *testExpectation;
    HighSchoolDirectoryProcessor *highSchoolDirectoryProcessor;
    ScoresProcessor *scoresProcessor;
}

@end

@implementation NYCHSTests

- (void)setUp {
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
}

- (void)testHighSchoolDirectoryProcessor {
    highSchoolDirectoryProcessor = [[HighSchoolDirectoryProcessor alloc] init];
    highSchoolDirectoryProcessor.delegate = self;
    [highSchoolDirectoryProcessor downloadData];

    testExpectation = [self expectationWithDescription:@"testHighSchoolDirectoryProcessor"];
    [self waitForExpectationsWithTimeout:10 handler:^(NSError *error) {
        if (error) {
            NSLog(@"testHighSchoolDirectoryProcessor error: %@", error);
        }
    }];
    
    XCTAssert(YES, @"Pass");
}

- (void)highSchoolDirectoryProcessorFinished:(NSArray *)schools
{
    XCTAssertNotNil(schools, @"Schools object is nil");
    XCTAssertNotEqual(schools.count, 0, @"Schools object does not have any records");
    School *school = [School findSchool:schools dbn:@"03M307"];
    XCTAssert([school.school_name isEqualToString:@"Urban Assembly School for Media Studies, The"], @"Unexpected data");
    [testExpectation fulfill];
}

- (void)highSchoolDirectoryProcessorError:(NSError *)error
{
    XCTFail(@"%@", error.localizedDescription);
}

- (void)testScoresProcessor {
    scoresProcessor = [[ScoresProcessor alloc] init];
    scoresProcessor.delegate = self;
    [scoresProcessor downloadData];
    
    testExpectation = [self expectationWithDescription:@"testScoresProcessor"];
    [self waitForExpectationsWithTimeout:10 handler:^(NSError *error) {
        if (error) {
            NSLog(@"testScoresProcessor error: %@", error);
        }
    }];
    
    XCTAssert(YES, @"Pass");
}

- (void)scoresProcessorFinished:(NSArray *)scores
{
    XCTAssertNotNil(scores, @"Scores object is nil");
    XCTAssertNotEqual(scores.count, 0, @"Scores object does not have any records");
    Scores *score = [Scores findScores:scores dbn:@"10X475"];
    XCTAssert([score.sat_math_avg_score isEqualToString:@"362"], @"Unexpected data");
    [testExpectation fulfill];
}

- (void)scoresProcessorError:(NSError *)error
{
    XCTFail(@"%@", error.localizedDescription);
}

@end
