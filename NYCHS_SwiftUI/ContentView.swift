//
//  ContentView.swift
//  NYCHS_SwiftUI
//
//  Created by Brian Prescott on 9/11/21.
//  Copyright © 2021 Brian Prescott. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State var schoolInformation: [School] = []
    @State var showErrorAlert = false
    @State var errorMessage: String = ""
    @State var schoolScores: [SchoolScores] = []
    
    var body: some View {
        NavigationView {
            List(schoolInformation) { info in
                NavigationLink(destination: ScoresView(school: info, schoolScores: schoolScores)) {
                    Image(systemName: "graduationcap")
                    VStack(alignment: .leading) {
                        Text(info.school_name)
                        HStack {
                            Text(info.city)
                                .font(.subheadline)
                            Spacer()
                            if let borough = info.borough {
                                Text("(\(borough.trimmingCharacters(in: .whitespaces)))")
                                    .font(.subheadline)
                            } else {
                                Text("(Unknown borough)")
                            }
                        }
                    }
                }
            }
            .navigationTitle(Text("New York City Schools"))
            .alert(isPresented: $showErrorAlert, content: { () -> Alert in
                Alert(title: Text("Error"), message: Text(errorMessage), dismissButton: .default(Text("OK")))
            })
        }
        .onAppear(perform: fetchSchoolInformation)
    }
    
    private func fetchSchoolInformation() {
        School.download(completion: { array, error in
            if let error = error {
                errorMessage = error.localizedDescription
                showErrorAlert = true
            } else if let array = array {
                schoolInformation = array.sorted { $0.school_name < $1.school_name }
                fetchSchoolScores()
            }
        })
    }
    
    private func fetchSchoolScores() {
        SchoolScores.download(completion: { array, error in
            if let error = error {
                errorMessage = error.localizedDescription
                showErrorAlert = true
            } else if let array = array {
                schoolScores = array
            }
        })
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
