//
//  NYCHS_SwiftUIApp.swift
//  NYCHS_SwiftUI
//
//  Created by Brian Prescott on 9/11/21.
//  Copyright © 2021 Brian Prescott. All rights reserved.
//

import SwiftUI

@main
struct NYCHS_SwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
