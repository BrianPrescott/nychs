//
//  School.swift
//  NYCHS_SwiftUI
//
//  Created by Brian Prescott on 9/11/21.
//  Copyright © 2021 Brian Prescott. All rights reserved.
//

import Foundation

let schoolDataUrl = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"

struct School: Identifiable, Decodable {
    var id = UUID()
    
    var borough: String?
    var city: String
    var dbn: String
    var overview_paragraph: String
    var school_name: String
    
    private enum CodingKeys: String, CodingKey {
        case borough, city, dbn, overview_paragraph, school_name
    }
    
    static func download(completion: @escaping ([School]?, Error?) -> Void) {
        let session = URLSession.shared
        guard let url = URL(string: schoolDataUrl) else { return }
        let task = session.dataTask(with: url) { data, response, error in
            if let error = error {
                if (error.localizedDescription == "The Internet connection appears to be offline.") {
                    guard let fileUrl = Bundle.main.url(forResource: "schools", withExtension: "json") else { return }
                    do {
                        let jsonData = try Data(contentsOf: fileUrl)
                        decodeData(data: jsonData, completion: completion)
                    } catch let error {
                        completion(nil, error)
                    }
                } else {
                    completion(nil, error)
                }
            } else if let data = data {
                decodeData(data: data, completion: completion)
            } else {
                // throw some kind of unexpected error
            }
        }
        
        task.resume()
    }
    
    static func decodeData(data: Data, completion: @escaping ([School]?, Error?) -> Void) {
        do {
            let decoder = JSONDecoder()
            let array = try decoder.decode([School].self, from: data)
            completion(array, nil)
        } catch let error {
            completion(nil, error)
        }
    }
}
