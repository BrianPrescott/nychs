//
//  SchoolScores.swift
//  NYCHS_SwiftUI
//
//  Created by Brian Prescott on 9/13/21.
//  Copyright © 2021 Brian Prescott. All rights reserved.
//

import Foundation

let schoolScoresDataUrl = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"

struct SchoolScores: Identifiable, Decodable {
    var id = UUID()
    var dbn: String
    var sat_critical_reading_avg_score: String
    var sat_math_avg_score: String
    var sat_writing_avg_score: String

    private enum CodingKeys: String, CodingKey {
        case dbn, sat_critical_reading_avg_score, sat_math_avg_score, sat_writing_avg_score
    }
    
    static func download(completion: @escaping ([SchoolScores]?, Error?) -> Void) {
        let session = URLSession.shared
        guard let url = URL(string: schoolScoresDataUrl) else { return }
        let task = session.dataTask(with: url) { data, response, error in
            if let error = error {
                if (error.localizedDescription == "The Internet connection appears to be offline.") {
                    guard let fileUrl = Bundle.main.url(forResource: "scores", withExtension: "json") else { return }
                    do {
                        let jsonData = try Data(contentsOf: fileUrl)
                        decodeData(data: jsonData, completion: completion)
                    } catch let error {
                        completion(nil, error)
                    }
                } else {
                    completion(nil, error)
                }
            } else if let data = data {
                decodeData(data: data, completion: completion)
            } else {
                // throw some kind of unexpected error
            }
        }
        
        task.resume()
    }
    
    static func decodeData(data: Data, completion: @escaping ([SchoolScores]?, Error?) -> Void) {
        do {
            let decoder = JSONDecoder()
            let array = try decoder.decode([SchoolScores].self, from: data)
            completion(array, nil)
        } catch let error {
            completion(nil, error)
        }
    }
}
