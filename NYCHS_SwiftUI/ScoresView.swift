//
//  ScoresView.swift
//  NYCHS_SwiftUI
//
//  Created by Brian Prescott on 9/13/21.
//  Copyright © 2021 Brian Prescott. All rights reserved.
//

import SwiftUI

struct ScoresView: View {
    let school: School!
    let schoolScores: [SchoolScores]!
    
    var body: some View {
        Text(school.school_name)
            .padding(.bottom, 10)
        Divider()
            .frame(height: 1)
            .background(Color.black)
        if let scores = schoolScores?.filter({$0.dbn == school.dbn}).first {
            Text("SAT critical reading average score: \(scores.sat_critical_reading_avg_score)")
                .padding(.top, 10)
            Text("SAT writing average score: \(scores.sat_writing_avg_score)")
            Text("SAT math average score: \(scores.sat_math_avg_score)")
                .padding(.bottom, 10)
        } else {
            Text("No SAT scores found")
                .padding(.top, 10)
                .padding(.bottom, 10)
        }
        Divider()
            .frame(height: 1)
            .background(Color.black)
        Text(school.overview_paragraph)
            .font(.subheadline)
            .padding(.top, 10)
            .padding(.leading, 20)
            .padding(.trailing, 20)
    }
}

struct ScoresView_Previews: PreviewProvider {
    static var previews: some View {
        ScoresView(school: nil, schoolScores: nil)
    }
}
