//
//  NYCHS_SwiftUITests.swift
//  NYCHS_SwiftUITests
//
//  Created by Brian Prescott on 9/13/21.
//  Copyright © 2021 Brian Prescott. All rights reserved.
//

import XCTest

class NYCHS_SwiftUITests: XCTestCase {

    var testExpectation: XCTestExpectation!
    
    let schoolsMagicDsn = "03M307"
    let scoresMagicDsn = "10X475"
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testSchoolInformationDownload() throws {
        testExpectation = self.expectation(description: "testSchoolInformationDownload")
        
        School.download(completion: { array, error in
            if let error = error {
                XCTFail("Error occurred during school information download: \(error.localizedDescription)")
            } else if let array = array {
                XCTAssert(array.count > 0, "School information array has 0 elements")
                if let school = array.filter({$0.dbn == self.schoolsMagicDsn}).first {
                    XCTAssert(school.school_name == "Urban Assembly School for Media Studies, The", "Unexpected data")
                } else {
                    XCTFail("Could not find school with DSN of \(self.schoolsMagicDsn)")
                }
                
                self.testExpectation.fulfill()
            } else {
                XCTFail("Array is nil")
            }
        })

        self.wait(for: [testExpectation], timeout: 30)

        XCTAssert(true, "Pass")
    }

    func testSchoolScoresDownload() throws {
        testExpectation = self.expectation(description: "testSchoolScoresDownload")
        
        SchoolScores.download(completion: { array, error in
            if let error = error {
                XCTFail("Error occurred during school scores download: \(error.localizedDescription)")
            } else if let array = array {
                XCTAssert(array.count > 0, "School scores array has 0 elements")
                if let scores = array.filter({$0.dbn == self.scoresMagicDsn}).first {
                    XCTAssert(scores.sat_math_avg_score == "362", "Unexpected data")
                } else {
                    XCTFail("Could not find scores with DSN of \(self.scoresMagicDsn)")
                }
                
                self.testExpectation.fulfill()
            } else {
                XCTFail("Array is nil")
            }
        })

        self.wait(for: [testExpectation], timeout: 30)

        XCTAssert(true, "Pass")
    }

}
