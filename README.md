# NYC Schools Challenge

## History

### First iteration
I first put this code together in Objective-C for an interview in early 2019.
There is an app target and a test target that can be run from within Xcode.

### Second iteration
After looking at the code to see if I could improve it for a potential interview in 2021, I decided to scrap the Objective-C code and put together a SwiftUI app.
This code now has a SwiftUI app target and a test target that are in the same project as the first iteration Objective-C code.

## Challenge specifications

### Description
Create an iOS app that provides information on NYC High schools. Display a list of NYC High Schools.

### School List Source API
Get your data here - https://data.cityofnewyork.us/Education/DOE-High-School-Directory-2017/s3k6-pzi2

Direct link - https://data.cityofnewyork.us/resource/s3k6-pzi2.json

### SAT Source API
SAT data here - https://data.cityofnewyork.us/Education/SAT-Results/f9bf-2cp4

Direct link - https://data.cityofnewyork.us/resource/f9bf-2cp4.json

### UI Note
Selecting a school should show additional information about the school. Display all the SAT scores - include Math, Reading and Writing.

### What are we looking for from this exercise?
1. A fully working prototype.
2. Don’t worry about making the UI pretty.
3. Tech approach you took and why.
4. What was important to you – look and feel or the functionality?
5. Unit test coverage.
